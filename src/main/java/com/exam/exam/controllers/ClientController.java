package com.exam.exam.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.exam.exam.entities.Client;
import com.exam.exam.repositories.ClientRepository;
import com.exam.exam.services.ClientService;

@RestController
@RequestMapping("api/v1/client")
public class ClientController {
    

    @Autowired
    private ClientRepository clientRepository ;
    @Autowired ClientService clientService;




    @PostMapping("save")
    public Client savClient(@RequestBody Client client){
        return this.clientRepository.save(client);
    }


    @GetMapping("all")
    public List<Client> getAllClient(){
        return this.clientRepository.findAll();
    }


    @PutMapping("update/{id}")
    public Client updateClient(@PathVariable Long id, @RequestBody Client client){
        return this.clientService.updateClient(id, client);
    }

    @DeleteMapping("delete/{id}")
    public void deleteClient(@PathVariable Long id){
        this.clientRepository.deleteById(id);
    }




    @PostMapping("addcategorie")
    public Client addClientoCategrie( @RequestParam Long clientid, @RequestParam String catid){
        return this.clientService.addClientToCategorie(clientid, catid);
    }

    @DeleteMapping("removecategorie")

    public Client removeCategorie(@RequestParam Long clientid, @RequestParam String catid){
        return this.clientService.removeCategorie(clientid, catid);
    }



    

}
