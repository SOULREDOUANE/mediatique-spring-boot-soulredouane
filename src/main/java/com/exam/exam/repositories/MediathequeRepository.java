package com.exam.exam.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.exam.exam.entities.Mediatheque;

public interface MediathequeRepository extends JpaRepository<Mediatheque,Long>{
    
}
