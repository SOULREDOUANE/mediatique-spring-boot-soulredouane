package com.exam.exam.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.exam.exam.entities.FicheEmprunt;
import java.util.List;
import java.util.Date;


public interface FicheEmpruntRepository extends JpaRepository<FicheEmprunt,Long> {
    List<FicheEmprunt> findByDateEmprunt(Date dateEmprunt);
    List<FicheEmprunt> findByDateLimite(Date dateLimite);
    List<FicheEmprunt> findByDateRappel(Date dateRappel);

    // List<FicheEmprunt> findByDepasse(Boolean depasse);
    
}
