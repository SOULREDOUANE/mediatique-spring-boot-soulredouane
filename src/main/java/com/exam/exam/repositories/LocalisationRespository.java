package com.exam.exam.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.exam.exam.entities.Localisation;

public interface LocalisationRespository  extends JpaRepository<Localisation,Long>{
    
}
