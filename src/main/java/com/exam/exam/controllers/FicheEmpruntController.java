package com.exam.exam.controllers;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.exam.exam.entities.FicheEmprunt;
import com.exam.exam.repositories.FicheEmpruntRepository;
import com.exam.exam.services.FicheEmpruntService;

@RestController
@RequestMapping("api/v1/ficheEmprunt")
public class FicheEmpruntController {



    @Autowired
    private FicheEmpruntRepository ficheEmpruntRepository;
    @Autowired 
    private FicheEmpruntService ficheEmpruntService;



    // @PostMapping("adddocument/{id}")
    // public void adddocument()


    @PostMapping("save")

    public FicheEmprunt savFicheEmprunt(@RequestBody FicheEmprunt ficheEmprunt){
        return this.ficheEmpruntRepository.save(ficheEmprunt);
    }


    @PutMapping("update/{id}")
    public FicheEmprunt updaFicheEmprunt(@PathVariable Long id, @RequestBody FicheEmprunt ficheEmprunt){
        return this.ficheEmpruntService.updateFicheEmprunte(id, ficheEmprunt);
    }
    

    @GetMapping("all")
    public  List<FicheEmprunt> getAllFicheEmprunts(){
        return this.ficheEmpruntRepository.findAll();
    }




    @DeleteMapping("delete")
    public void deleteFicheEmprunt(@RequestParam Long id){
        this.ficheEmpruntRepository.deleteById(id);
    }


    @GetMapping("get/dateemprunt")
    public List<FicheEmprunt> getByDateEmprunt(@RequestParam Date date){
            return this.ficheEmpruntRepository.findByDateEmprunt(date);
    }

    @GetMapping("get/datelimite")
    public List<FicheEmprunt> getByDateLimite(@RequestParam Date date){
            return this.ficheEmpruntRepository.findByDateLimite(date);
    }

    @GetMapping("get/daterappel")
    public List<FicheEmprunt> getByDateReppel(@RequestParam Date date){
            return this.ficheEmpruntRepository.findByDateRappel(date);
    }


    // @GetMapping("get/dateemprunt")
    // public List<FicheEmprunt> getByDateReppel(@RequestParam Boolean tarif){
    //         return this.ficheEmpruntRepository.findByDepasse(tarif);
    // }



    

}
