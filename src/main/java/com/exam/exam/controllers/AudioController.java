package com.exam.exam.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import com.exam.exam.entities.Audio;
import com.exam.exam.repositories.AudioRepository;
import com.exam.exam.services.AudioService;

@RestController
@RequestMapping("api/v1/audio")
public class AudioController {

    @Autowired 
    private AudioRepository audioRepository ;
    @Autowired
    private AudioService audioService;


    @GetMapping("find/{classification}")
    public List<Audio> findByName(@PathVariable String classification){
        return this.audioRepository.findByClassification(classification);
    }
    

    @PostMapping("save")
    public Audio saveAudio(@RequestBody Audio audio){

        return this.audioRepository.save(audio);

    }


    @GetMapping("get/{id}")
    public Audio getAudioByd(@PathVariable String id){
        return this.audioRepository.findById(id).get();
    }


    @GetMapping("all")
    public List<Audio> getAll(){
        return this.audioRepository.findAll();
    }
    @GetMapping("get/annee")
    public List<Audio> getByAnnee(@RequestParam String annee){
        return this.audioRepository.findByAnnee(annee);
    }
    @GetMapping("get/titre")
    public List<Audio> getByTitre(@RequestParam String titre){
        return this.audioRepository.findByTitre(titre);
    }
    @GetMapping("get/emprunte")
    public List<Audio> getByEmprunte(@RequestParam Boolean emprunte){
        return this.audioRepository.findByEmprunte(emprunte);
    }
    @GetMapping("get/empruntetable")
    public List<Audio> getByEmprunteTable(@RequestParam Boolean empruntetable){
        return this.audioRepository.findByEmpruntTable(empruntetable);
    }



    @DeleteMapping("delete/{id}")
    public void deleteAudio(@PathVariable String id){
        this.audioRepository.deleteById(id);
    }


    @PutMapping("update/{id}")
    public Audio updateAudio(@PathVariable String id, @RequestBody Audio audio){
        return this.audioService.updateAudio(id,audio);
    }

    


    
    

    



    
}
