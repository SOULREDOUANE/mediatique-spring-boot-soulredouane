package com.exam.exam.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.exam.exam.entities.Client;

public interface ClientRepository extends JpaRepository<Client,Long> {
    
}
