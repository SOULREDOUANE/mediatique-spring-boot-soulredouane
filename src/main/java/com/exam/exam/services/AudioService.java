package com.exam.exam.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.exam.exam.entities.Audio;
import com.exam.exam.repositories.AudioRepository;

@Service
public class AudioService {


    @Autowired
    private AudioRepository audioRepository;



    public Audio updateAudio(String code,Audio audio){
        return this.audioRepository.findById(code)
        .map(aud-> {
            aud.setAnnee(audio.getAnnee());
            aud.setAuteur(audio.getAuteur());
            aud.setClassification(audio.getClassification());
            aud.setEmprunte(audio.getEmprunte());
            aud.setEmpruntTable(audio.getEmpruntTable());
            aud.setNbEmprunts(audio.getNbEmprunts());
            aud.setTitre(audio.getTitre());
            aud.setCode(audio.getCode());
            return this.audioRepository.save(aud);
        }).orElseGet(()->this.audioRepository.save(audio));
    }
    
}
