package com.exam.exam.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.exam.exam.entities.CategorieClient;

public interface CategorieClientRepository  extends JpaRepository<CategorieClient,String>{
    
}
