package com.exam.exam.entities;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.List;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.OneToMany;

@Entity
@Component
public class CategorieClient {
    @Id 
    @Column(unique = true)
    private String  nomCat;
    private Integer nbEmpruntsMax;
    private double cotisation;
    private double coefTarif;
    private double coefDuree;
    private Boolean codeReductionActif;

    @JsonIgnore
    @ManyToMany
    private List<Client> clients;




    

    
    public CategorieClient() {
    }

    
    public CategorieClient(String nomCat, Integer nbEmpruntsMax, double cotisation, double coefTarif, double coefDuree,
            Boolean codeReductionActif) {
        this.nomCat = nomCat;
        this.nbEmpruntsMax = nbEmpruntsMax;
        this.cotisation = cotisation;
        this.coefTarif = coefTarif;
        this.coefDuree = coefDuree;
        this.codeReductionActif = codeReductionActif;
    }
    public String getNomCat() {
        return nomCat;
    }
    public Integer getNbEmpruntsMax() {
        return nbEmpruntsMax;
    }
    public double getCotisation() {
        return cotisation;
    }
    public double getCoefTarif() {
        return coefTarif;
    }
    public double getCoefDuree() {
        return coefDuree;
    }
    public Boolean getCodeReductionActif() {
        return codeReductionActif;
    }





    
    
}
