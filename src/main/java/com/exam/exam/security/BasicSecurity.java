package com.exam.exam.security;

import java.lang.reflect.Method;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
@EnableWebSecurity
public class BasicSecurity {





    @Bean
    SecurityFilterChain filterChain(HttpSecurity httpSecurity) throws Exception{

        return httpSecurity.csrf(c->c.disable())
        .authorizeHttpRequests(auth->
        auth.requestMatchers(HttpMethod.DELETE,"api/v1/genre/delete/**").hasRole("ADMIN")
        .requestMatchers(HttpMethod.POST,"api/v1/genre/save").hasRole("ADMIN")
        .requestMatchers(HttpMethod.PUT,"api/v1/genre/update/**").hasRole("ADMIN")
        .requestMatchers(HttpMethod.GET,"api/v1/genre/all").hasRole("USER")
        .requestMatchers(HttpMethod.DELETE,"api/v1/ficheEmprunt/delete/**").hasRole("ADMIN")
        .requestMatchers(HttpMethod.PUT,"api/v1/ficheEmprunt/update/**").hasRole("ADMIN")
        .requestMatchers(HttpMethod.POST,"api/v1/ficheEmprunt/save").hasRole("ADMIN")
        .requestMatchers(HttpMethod.GET,"api/v1/ficheEmprunt/all").hasRole("USER")
        .requestMatchers(HttpMethod.PUT,"api/v1/client/update/**").hasRole("ADMIN")
        .requestMatchers(HttpMethod.POST,"api/v1/client/save").hasRole("ADMIN")
        .requestMatchers(HttpMethod.DELETE,"api/v1/client/delete/**").hasRole("ADMIN")
        .requestMatchers(HttpMethod.GET,"api/v1/client/all").hasRole("USER")
        
        .anyRequest().authenticated()

        ).httpBasic(h->h.realmName("http basic for exam")).build();

    }
    



    @Bean
    InMemoryUserDetailsManager saveUsers(){

       User user = (User) User.builder()
                .username("user")
                .password(passwordEncoder().encode("soul"))
                .roles("USER").build();
                
       User admin= (User) User.builder()
                .username("admin")
                .password(passwordEncoder().encode("soul"))
                .roles("ADMIN").build();
                



        return new InMemoryUserDetailsManager(user,admin);
    }

    



    @Bean
    BCryptPasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }
}
