package com.exam.exam.entities;

import org.springframework.stereotype.Component;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Inheritance;
import jakarta.persistence.InheritanceType;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToOne;

@Entity
@Component
@Inheritance(strategy =  InheritanceType.JOINED)
public abstract class Document {

    @Id 
    @Column(nullable = false, unique = true)
    private String code;
    private String titre;
    private String auteur;
    private String annee;
    private Boolean empruntTable=false;
    private Boolean emprunte =false;
    private Integer nbEmprunts=0;

    @ManyToOne
    // @JoinColumn(name="genre_id")
    private Genre genre;

    @ManyToOne
    private Localisation localisation;

    // @OneToOne 
    // private FicheEmprunt ficheEmprunt;


    public Document(){}
    
    public Document(String code, String titre, String auteur, String annee, Boolean empruntTable, Boolean emprunte,
            Integer nbEmprunts) {
        this.code = code;
        this.titre = titre;
        this.auteur = auteur;
        this.annee = annee;
        this.empruntTable = empruntTable;
        this.emprunte = emprunte;
        this.nbEmprunts = nbEmprunts;
    }


    

   

    public String getCode() {
        return code;
    }
    public void setCode(String code) {
        this.code = code;
    }
    public String getTitre() {
        return titre;
    }
    public void setTitre(String titre) {
        this.titre = titre;
    }
    public String getAuteur() {
        return auteur;
    }
    public void setAuteur(String auteur) {
        this.auteur = auteur;
    }
    public Boolean getEmpruntTable() {
        return empruntTable;
    }
    public void setEmpruntTable(Boolean empruntTable) {
        this.empruntTable = empruntTable;
    }
    public Boolean getEmprunte() {
        return emprunte;
    }
    public void setEmprunte(Boolean emprunte) {
        this.emprunte = emprunte;
    }
    public Integer getNbEmprunts() {
        return nbEmprunts;
    }
    public void setNbEmprunts(Integer nbEmprunts) {
        this.nbEmprunts = nbEmprunts;
    }

    public String getAnnee() {
        return annee;
    }

    public void setAnnee(String annee) {
        this.annee = annee;
    }

    public Genre getGenre() {
        return genre;
    }

    public void setGenre(Genre genre) {
        this.genre = genre;
    }

    public Localisation getLocalisation() {
        return localisation;
    }

    public void setLocalisation(Localisation localisation) {
        this.localisation = localisation;
    }


    


    

    
}
