package com.exam.exam.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.exam.exam.entities.Livre;
import com.exam.exam.repositories.LivreRespository;

@Service
public class LivreService {


    @Autowired
    private LivreRespository livreRespository;


    public Livre updateLivre(String code , Livre livre){
        return this.livreRespository.findById(code)
        .map(liv->{
            liv.setCode(livre.getCode());
            liv.setAnnee(livre.getAnnee());
            liv.setAuteur(livre.getAuteur());
            liv.setEmpruntTable(livre.getEmpruntTable());
            liv.setEmprunte(livre.getEmprunte());
            liv.setNombrePage(livre.getNombrePage());
            liv.setNbEmprunts(livre.getNbEmprunts());
            return this.livreRespository.save(liv);
        }).orElseGet(()->this.livreRespository.save(livre));
    }
    
}
