package com.exam.exam.entities;

import java.util.Date;

import org.springframework.stereotype.Component;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToOne;

@Entity 
@Component
public class FicheEmprunt {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)

    private Long id;

    private Date dateEmprunt;
    private Date dateLimite;
    private Date dateRappel;
    private Boolean Depasse;
    private double tarif;

    @OneToOne
    private Document document;

    @ManyToOne
    private Client client;




    public FicheEmprunt(){}



    
    public FicheEmprunt(Date dateEmprunt, Date dateLimite, Date dateRappel, Boolean depasse, double tarif) {
        this.dateEmprunt = dateEmprunt;
        this.dateLimite = dateLimite;
        this.dateRappel = dateRappel;
        Depasse = depasse;
        this.tarif = tarif;
    }
    public Date getDateEmprunt() {
        return dateEmprunt;
    }
    public void setDateEmprunt(Date dateEmprunt) {
        this.dateEmprunt = dateEmprunt;
    }
    public Date getDateLimite() {
        return dateLimite;
    }
    public void setDateLimite(Date dateLimite) {
        this.dateLimite = dateLimite;
    }
    public Date getDateRappel() {
        return dateRappel;
    }
    public void setDateRappel(Date dateRappel) {
        this.dateRappel = dateRappel;
    }
    public Boolean getDepasse() {
        return Depasse;
    }
    public void setDepasse(Boolean depasse) {
        Depasse = depasse;
    }
    public double getTarif() {
        return tarif;
    }
    public void setTarif(double tarif) {
        this.tarif = tarif;
    }
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }



    
    
}
