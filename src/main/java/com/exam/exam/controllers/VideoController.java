package com.exam.exam.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

import com.exam.exam.entities.Video;
import com.exam.exam.repositories.VideoRepository;
import com.exam.exam.services.VideoService;
@RestController
@RequestMapping("api/v1/video")
public class VideoController {



    @Autowired
    private VideoRepository videoRepository;
    @Autowired
    private VideoService videoService;



    @GetMapping("all")

    public List<Video> findAll(){
        return this.videoRepository.findAll();
    }


    @PostMapping("save")
    public Video savVideo(@RequestBody Video video){
        return this.videoRepository.save(video);
    }


    @PutMapping("update/{id}")
    public Video upadatVideo(@PathVariable String id, @RequestBody Video video){
        return this.videoService.updateLivre(id, video);
    }





    @DeleteMapping("delete/{id}")
    public void deleteVideo(@PathVariable String id){
        this.videoRepository.deleteById(id);
    }


    @GetMapping("get/titre")
    public List<Video> findByTitre(@RequestParam String titre){
        return this.videoRepository.findByTitre(titre);
    }
    @GetMapping("get/annee")
    public List<Video> findByAnne(@RequestParam String annee){
        return this.videoRepository.findByAnnee(annee);
    }
    @GetMapping("get/emprunte")
    public List<Video> findByEmprunte(@RequestParam Boolean emprunte){
        return this.videoRepository.findByEmprunte(emprunte);
    }
    @GetMapping("get/empruntetable")
    public List<Video> findByEmprunteTable(@RequestParam Boolean empruntetable){
        return this.videoRepository.findByEmpruntTable(empruntetable);
    }
    @GetMapping("get/metionlegal")
    public List<Video> findByMentionLegal(@RequestParam String metionlegal){
        return this.videoRepository.findByMentionLegale(metionlegal);
    }
    @GetMapping("get")
    public List<Video> findByAuteur(@RequestParam String auteur){
        return this.videoRepository.findByAuteur(auteur);
    }
    

    
}
