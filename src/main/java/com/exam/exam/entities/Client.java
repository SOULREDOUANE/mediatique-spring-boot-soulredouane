package com.exam.exam.entities;

import java.util.ArrayList;
import java.util.Date;

import org.springframework.stereotype.Component;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToMany;

import java.util.List;

@Entity
@Component
public class Client {

    @Id @GeneratedValue(strategy =  GenerationType.IDENTITY)
    private Long id;
    private String nom;
    private String prenom;

    private String address;
    private Date dateInscription;
    private Date dateRenouvellement;
    private Integer nbEmpruntEffectues=0;
    private Integer nbEmpruntDeppasse=0;
    private Integer nbEmpruntEncours=0;
    private Integer codeReduction;

    @ManyToMany
    private List<CategorieClient> categorieClient= new  ArrayList<>();
    



    public Client(){}
    
    public Client( Integer nbEmpruntEffectues, Integer nbEmpruntDeppasse, Integer nbEmpruntEncours) {
    
        this.nbEmpruntEffectues = nbEmpruntEffectues;
        this.nbEmpruntDeppasse = nbEmpruntDeppasse;
        this.nbEmpruntEncours = nbEmpruntEncours;
    }
    public String getNom() {
        return nom;
    }
    public void setNom(String nom) {
        this.nom = nom;
    }
    public String getPrenom() {
        return prenom;
    }
    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }
    public String getAddress() {
        return address;
    }
    public void setAddress(String address) {
        this.address = address;
    }
    public Date getDateInscription() {
        return dateInscription;
    }
    public void setDateInscription(Date dateInscription) {
        this.dateInscription = dateInscription;
    }
    public Date getDateRenouvellement() {
        return dateRenouvellement;
    }
    public void setDateRenouvellement(Date dateRenouvellement) {
        this.dateRenouvellement = dateRenouvellement;
    }
    public Integer getNbEmpruntEffectues() {
        return nbEmpruntEffectues;
    }
    public void setNbEmpruntEffectues(Integer nbEmpruntEffectues) {
        this.nbEmpruntEffectues = nbEmpruntEffectues;
    }
    public Integer getNbEmpruntDeppasse() {
        return nbEmpruntDeppasse;
    }
    public void setNbEmpruntDeppasse(Integer nbEmpruntDeppasse) {
        this.nbEmpruntDeppasse = nbEmpruntDeppasse;
    }
    public Integer getNbEmpruntEncours() {
        return nbEmpruntEncours;
    }
    public void setNbEmpruntEncours(Integer nbEmpruntEncours) {
        this.nbEmpruntEncours = nbEmpruntEncours;
    }
    public Integer getCodeReduction() {
        return codeReduction;
    }
    public void setCodeReduction(Integer codeReduction) {
        this.codeReduction = codeReduction;
    }

    public List<CategorieClient> getCategorieClient() {
        return categorieClient;
    }

    public void setCategorieClient(List<CategorieClient> categorieClient) {
        this.categorieClient = categorieClient;
    }


    



    

    
}
