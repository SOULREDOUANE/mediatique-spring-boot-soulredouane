package com.exam.exam.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import com.exam.exam.entities.Genre;
import com.exam.exam.repositories.GenreRepository;
import com.exam.exam.services.GenreService;

@RestController
@RequestMapping("api/v1/genre")
public class GenreController {


    @Autowired 
    private GenreRepository genreRepository;

    @Autowired
    private GenreService genreService;



    @PostMapping("save")
    public Genre saveGenre(@RequestBody Genre genre){
        return this.genreRepository.save(genre);
    }

    @GetMapping("all")
    public List<Genre> getAllGenre(){
        return this.genreRepository.findAll();
    }
    
    @DeleteMapping("delete/{id}")

    public void deleteGenre(@PathVariable Long id){
            this.genreRepository.deleteById(id);
    }

    @PutMapping("update/{id}")
    public Genre updateGenre(@PathVariable Long id, @RequestBody Genre genre){
        return this.genreService.updatGenre(id, genre);
    }


   
    
}
