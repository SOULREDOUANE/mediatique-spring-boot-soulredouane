package com.exam.exam.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.exam.exam.entities.Video;
import com.exam.exam.repositories.VideoRepository;

@Service
public class VideoService {


    @Autowired
    private VideoRepository videoRepository;


    public Video updateLivre(String code , Video video){
        return this.videoRepository.findById(code)
        .map(vid->{
            vid.setCode(video.getCode());
            vid.setAnnee(video.getAnnee());
            vid.setAuteur(video.getAuteur());
            vid.setEmpruntTable(video.getEmpruntTable());
            vid.setEmprunte(video.getEmprunte());
            vid.setDureeFilme(video.getDureeFilme());
            vid.setMentionLegale(video.getMentionLegale());
            vid.setNbEmprunts(video.getNbEmprunts());
            return this.videoRepository.save(vid);
        }).orElseGet(()->this.videoRepository.save(video));
    }
    
}
