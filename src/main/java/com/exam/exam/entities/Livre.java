package com.exam.exam.entities;

import org.springframework.stereotype.Component;

import jakarta.persistence.Entity;

@Entity
@Component
public class Livre extends Document {

    private Integer nombrePage;
    private Integer DURRE = 6;
    private double TARIF = .5;
    

    public Livre(){}
    


    public Livre(String code, String titre, String auteur,String annee, Boolean empruntTable, Boolean emprunte, Integer nbEmprunts,
            Integer nombrePage) {
        super(code, titre, auteur, annee, empruntTable, emprunte, nbEmprunts);
        this.nombrePage = nombrePage;
    }


    



   



    public Integer getNombrePage() {
        return nombrePage;
    }


    public void setNombrePage(Integer nombrePage) {
        this.nombrePage = nombrePage;
    }


    public Integer getDURRE() {
        return DURRE;
    }


    public void setDURRE(Integer dURRE) {
        DURRE = dURRE;
    }


    public double getTARIF() {
        return TARIF;
    }


    public void setTARIF(double tARIF) {
        TARIF = tARIF;
    }

    
}
