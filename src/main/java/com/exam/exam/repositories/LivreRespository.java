package com.exam.exam.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.exam.exam.entities.Livre;

public interface LivreRespository extends JpaRepository<Livre,String> {

    
} 
