package com.exam.exam.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import com.exam.exam.entities.Localisation;
import com.exam.exam.repositories.LocalisationRespository;
import com.exam.exam.services.LocalistionService;

@RestController
@RequestMapping("api/v1/localisation")
public class LocalistionController {

    @Autowired
    private LocalistionService localistionService;
    @Autowired
    private LocalisationRespository localisationRespository;
    


    @PostMapping("save")
    public Localisation saveLocalisation(@RequestBody Localisation localisation){
        return this.localisationRespository.save(localisation);
    }


    @GetMapping("all")
    public List<Localisation> getAllLocalisation(){
        return this.localisationRespository.findAll();

    }

    @DeleteMapping("delete/{id}")
    public void delteLocalisation(@PathVariable Long id){
        this.localisationRespository.deleteById(id);
    }

    @PutMapping("update/{id}")
    public Localisation updateLocalisation(@PathVariable Long id, @RequestBody Localisation localisation){
        return this.localistionService.updLocalisation(id, localisation);
    }



}
