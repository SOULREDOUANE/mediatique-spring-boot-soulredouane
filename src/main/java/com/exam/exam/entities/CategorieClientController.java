package com.exam.exam.entities;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import com.exam.exam.repositories.CategorieClientRepository;

@RestController
@RequestMapping("api/v1/categorieclient")
public class CategorieClientController {



    @Autowired
    private CategorieClientRepository categorieClientRepository;


    @PostMapping("save")
    public CategorieClient saveCategorieClient(@RequestBody CategorieClient categorieClient){
        return this.categorieClientRepository.save(categorieClient);
    }


    @GetMapping("all")
    public List<CategorieClient>  gelAllCategorieClients(){
        return this.categorieClientRepository.findAll();
    }


    @DeleteMapping("delete/{id}")
    public void deleteCategorieClient(@PathVariable String id){
        this.categorieClientRepository.deleteById(id);
    }
}
