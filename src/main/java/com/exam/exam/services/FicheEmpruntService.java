package com.exam.exam.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.exam.exam.entities.FicheEmprunt;
import com.exam.exam.repositories.FicheEmpruntRepository;

@Service
public class FicheEmpruntService {

    @Autowired
    private FicheEmpruntRepository ficheEmpruntRepository;



    public FicheEmprunt updateFicheEmprunte(Long id,FicheEmprunt ficheEmprunt){
        return this.ficheEmpruntRepository.findById(id)
        .map(f->{f.setDateEmprunt(ficheEmprunt.getDateEmprunt());
            f.setDateLimite(ficheEmprunt.getDateLimite());
            f.setDateRappel(ficheEmprunt.getDateRappel());
            f.setDepasse(ficheEmprunt.getDepasse());
            f.setTarif(ficheEmprunt.getTarif());
            return this.ficheEmpruntRepository.save(f);
        } ).orElseGet(()->this.ficheEmpruntRepository.save(ficheEmprunt));
    }
    

    
}
