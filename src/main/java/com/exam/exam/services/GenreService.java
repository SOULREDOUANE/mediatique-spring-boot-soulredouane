package com.exam.exam.services;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.exam.exam.entities.Genre;
import com.exam.exam.repositories.GenreRepository;

@Service
public class GenreService {
    

    @Autowired
    private GenreRepository genreRepository;

    public Genre updatGenre(Long id,Genre genre){


        Genre ge = this.genreRepository.findById(id).orElseThrow();

        ge.setNbEmprunts(genre.getNbEmprunts());
        ge.setNom(genre.getNom());

        this.genreRepository.save(ge);

        return ge;

    }


   
}
