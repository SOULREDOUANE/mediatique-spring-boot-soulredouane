package com.exam.exam.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.exam.exam.entities.Localisation;
import com.exam.exam.repositories.LocalisationRespository;

@Service
public class LocalistionService {
    

    @Autowired
    private LocalisationRespository localisationRespository;



    public Localisation updLocalisation(Long id , Localisation localisation){
        return this.localisationRespository.findById(id).
        map(loc-> {
            loc.setRayon(localisation.getRayon());
            loc.setSalle(localisation.getSalle());

            return this.localisationRespository.save(loc);
        }).orElseGet(()->this.localisationRespository.save(localisation));
    }
}
