package com.exam.exam.entities;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.List;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;

@Entity
@Component
public class Genre {
    @Id @GeneratedValue(strategy =  GenerationType.IDENTITY)
    private Long id;
    private String nom;
    private Integer nbEmprunts=0;

    @JsonIgnore
    @OneToMany(mappedBy = "genre")
    private List<Document> Documents;

    public Genre(){}


    
    public Genre(String nom, Integer nbEmprunts) {
        this.nom = nom;
        this.nbEmprunts = nbEmprunts;
    }
    public String getNom() {
        return nom;
    }
    public void setNom(String nom) {
        this.nom = nom;
    }
    public Integer getNbEmprunts() {
        return nbEmprunts;
    }
    public void setNbEmprunts(Integer nbEmprunts) {
        this.nbEmprunts = nbEmprunts;
    }



    public List<Document> getDocuments() {
        return Documents;
    }



    public void setDocuments(List<Document> documents) {
        Documents = documents;
    }



    public Long getId() {
        return id;
    }



    public void setId(Long id) {
        this.id = id;
    }
    

    


    
    
}
