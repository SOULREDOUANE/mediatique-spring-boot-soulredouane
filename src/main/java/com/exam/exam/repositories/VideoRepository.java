package com.exam.exam.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.exam.exam.entities.Video;
import java.util.List;


public interface VideoRepository extends JpaRepository<Video,String> {

    List<Video> findByAnnee(String annee);
    List<Video> findByAuteur(String auteur);
    List<Video> findByTitre(String titre);
    List<Video> findByEmpruntTable(Boolean empruntTable);
    List<Video> findByEmprunte(Boolean emprunte);
    List<Video> findByMentionLegale(String mentionLegale);
    
    
}
