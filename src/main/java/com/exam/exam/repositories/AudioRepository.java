package com.exam.exam.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.exam.exam.entities.Audio;
import java.util.List;


public interface AudioRepository extends JpaRepository<Audio,String> {
    List<Audio> findByClassification(String classification);
    List<Audio> findByTitre(String titre);
    List<Audio> findByAuteur(String auteur);
    List<Audio> findByAnnee(String annee);
    List<Audio> findByEmpruntTable(Boolean empruntTable);
    List<Audio> findByEmprunte(Boolean emprunte);

    
    

    
} 