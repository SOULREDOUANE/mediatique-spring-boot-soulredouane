package com.exam.exam.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.exam.exam.entities.CategorieClient;
import com.exam.exam.entities.Client;
import com.exam.exam.repositories.CategorieClientRepository;
import com.exam.exam.repositories.ClientRepository;

@Service
public class ClientService {


    @Autowired
    private ClientRepository clientRepository;
    @Autowired 
    private CategorieClientRepository categorieClientRepository;



    public Client updateClient(Long id,Client client){
        return this.clientRepository.findById(id)
        .map(cl->{
            cl.setAddress(client.getAddress());
            cl.setCodeReduction(client.getCodeReduction());
            cl.setDateInscription(client.getDateInscription());
            cl.setDateRenouvellement(client.getDateRenouvellement());
            cl.setNbEmpruntDeppasse(client.getNbEmpruntDeppasse());
            cl.setNbEmpruntEffectues(client.getNbEmpruntEffectues());
            cl.setNom(client.getNom());
            cl.setPrenom(client.getPrenom());
            return this.clientRepository.save(cl);
        }).orElseGet(()-> this.clientRepository.save(client));
    }


    public Client addClientToCategorie(Long clientid, String catid){

        CategorieClient cat = this.categorieClientRepository.findById(catid).get();
        Client client = this.clientRepository.findById(clientid).get();
        client.getCategorieClient().add(cat);

        return this.clientRepository.save(client);

    }

    public Client removeCategorie(Long clientid, String catid){

        CategorieClient cat = this.categorieClientRepository.findById(catid).get();
        Client client = this.clientRepository.findById(clientid).get();

        int index = client.getCategorieClient().indexOf(cat);
        client.getCategorieClient().remove(index);

        return this.clientRepository.save(client);

    }




    
}
