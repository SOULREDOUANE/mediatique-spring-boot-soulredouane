package com.exam.exam.entities;

import java.util.List;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;

@Entity
@Component
public class Localisation {
    @Id  @GeneratedValue(strategy =  GenerationType.IDENTITY)
    private Long id;
    @Column(length = 70)
    private String salle;
    private String rayon;

    @JsonIgnore
    @OneToMany(mappedBy = "localisation")
    private List<Document> documents;


    public Localisation(){}
    




    public Localisation(String salle, String rayon) {
        this.salle = salle;
        this.rayon = rayon;
    }





    public String getSalle() {
        return salle;
    }
    public void setSalle(String salle) {
        this.salle = salle;
    }
    public String getRayon() {
        return rayon;
    }
    public void setRayon(String rayon) {
        this.rayon = rayon;
    }





    public List<Document> getDocuments() {
        return documents;
    }





    public void setDocuments(List<Document> documents) {
        this.documents = documents;
    }





    public Long getId() {
        return id;
    }





    public void setId(Long id) {
        this.id = id;
    }


    
    
    
}
