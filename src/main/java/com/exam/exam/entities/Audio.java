package com.exam.exam.entities;

import org.springframework.stereotype.Component;

import jakarta.persistence.Entity;

@Component
@Entity
public class Audio  extends Document {

    private String classification;
    private Integer DURRE = 4;
    private double TARIF = 1.0;


    public Audio(){}

    
    public Audio(String code, String titre, String auteur, String annee,  Boolean empruntTable, Boolean emprunte, Integer nbEmprunts,
            String classification) {
        super(code, titre, auteur,annee, empruntTable, emprunte, nbEmprunts);
        this.classification = classification;
    }
    public String getClassification() {
        return classification;
    }
    public void setClassification(String classification) {
        this.classification = classification;
    }
    public Integer getDURRE() {
        return DURRE;
    }
    public void setDURRE(Integer dURRE) {
        DURRE = dURRE;
    }
    public double getTARIF() {
        return TARIF;
    }
    public void setTARIF(double tARIF) {
        TARIF = tARIF;
    }


    
    
}
