package com.exam.exam.entities;


import org.springframework.stereotype.Component;

import jakarta.persistence.Entity;

@Component
@Entity
public class Video  extends Document{

    private Integer dureeFilme;
    private String mentionLegale;
    private Integer DUREE=2;
    private double TARIF =1.5;



    public Video(){}

    public Video(String code, String titre, String auteur, String annee, Boolean empruntTable, Boolean emprunte,
    Integer nbEmprunts){
        super( code,  titre,  auteur, annee, empruntTable, emprunte,
        nbEmprunts);
    }



    

    public Video(String code, String titre, String auteur, String annee, Boolean empruntTable, Boolean emprunte, Integer nbEmprunts,
            Integer dureeFilme, String mentionLegale) {
        super(code, titre, auteur, annee, empruntTable, emprunte, nbEmprunts);
        this.dureeFilme = dureeFilme;
        this.mentionLegale = mentionLegale;
    }

    public Integer getDureeFilme() {
        return dureeFilme;
    }

    public void setDureeFilme(Integer dureeFilme) {
        this.dureeFilme = dureeFilme;
    }

    public String getMentionLegale() {
        return mentionLegale;
    }

    public void setMentionLegale(String mentionLegale) {
        this.mentionLegale = mentionLegale;
    }

    public Integer getDUREE() {
        return DUREE;
    }

    public void setDUREE(Integer dUREE) {
        DUREE = dUREE;
    }

    public double getTARIF() {
        return TARIF;
    }

    public void setTARIF(double tARIF) {
        TARIF = tARIF;
    }


    
    
}
