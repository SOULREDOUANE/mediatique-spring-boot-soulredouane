package com.exam.exam.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import com.exam.exam.entities.Livre;
import com.exam.exam.repositories.LivreRespository;
import com.exam.exam.services.LivreService;

@RestController
@RequestMapping("api/v1/livre")
public class LivreController {

    @Autowired
    private LivreRespository livreRespository ;
    @Autowired
    private LivreService livreService;
    



    @PostMapping("save")

    public Livre  savLivre(@RequestBody Livre livre) {
        return this.livreRespository.save(livre);
    }

    @GetMapping("all")
    public List<Livre> getAllLivre(){
        return this.livreRespository.findAll();
    }



    @DeleteMapping("delete/{id}")
    public void deleteLivre(@PathVariable String id){
        this.livreRespository.deleteById(id);
    }


    @PutMapping("update/{id}")

    public Livre updateLivre(@PathVariable String id, @RequestBody Livre livre){
        return this.livreService.updateLivre(id, livre);
    }
}
